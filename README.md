# Getting Started with Create React App
#Added component StepProgress

#Commands to run project 

#Install package
npm install 

#Run project
npm start



#Description of test
React exercise
📖 Spec
Number of steps is dynamic
There's a minimum step of two and a maximum of five
You can't jump over a step
Write a simple test to check if the state has changed after click
🔥 tips
Make use of ES6
Use stateless components where possible
Demonstrate the understanding of React components' life cycle
Enhancing the component with simple animations is a nice to have
